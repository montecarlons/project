#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <math.h>
double samplesInsideCircle(const int sample_size){
	int counter=0,s,i=0;
	double x=0, y=0;
	for (s = 0; s < sample_size; s++){
		x = rand()/(double)RAND_MAX*2-1;
		y = rand()/(double)RAND_MAX*2-1;
		if (sqrt((x*x) + (y*y)) < 1){
			counter++;
		}
	}
	return counter;
}
double parallel_pi(void){
	//const int nsamples = 10000; //thats ten thousand
	//const int nsamples = 1000000; //thats one million 
	const int nsamples = 1000000000; //thats one billion 
	int num_chunks = 10;
	int chunk = nsamples / num_chunks;
	int counter = 0;
	int i,myTime=time(NULL);
	double approx_pi=0;
	#pragma omp parallel for
	for (i = 0; i < num_chunks; i++){
		srand (myTime*(omp_get_thread_num()+1));
		counter+=samplesInsideCircle(chunk);
	}
	approx_pi = 4.0 * counter / nsamples;
	return approx_pi;
}
int main(int argc, char **arg){
	double tstart,tend,elapsed,approx_pi;
	omp_set_dynamic(12);
        tstart = omp_get_wtime(); //gettime();
	approx_pi = parallel_pi();
	//printing
        tend = omp_get_wtime(); //gettime();
        elapsed = 1000*(tend-tstart);
        printf("Computed monte carlo pi=%e in %6.3f miliseconds\n", approx_pi, elapsed);
        return 0;
}	


