#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <math.h>
#include "nv_solver.h"
#define V(i,j) v[(i) + (j)*n]
#define W(i,j) w[(i) + (j)*n]
void nv_solver(double* v, double *w, int n, int x, int y, int grid_type,int tsteps,int alpha){ 
int i,j,k;
double xval, yval, deltax, deltay;
    
if(grid_type !=1){
	y=x;
}

deltax=(n/x);
deltay=(n/y);

/*for (i=0;i<x;i++){
    for(j=0;j<y;j++){
      V(i,j) = alpha * V(i,j);
    }
}
*/
if(grid_type == 0){
V(0,0) = 10* V(0,0);
for(k=0;k<tsteps;k++){
    for(i=0;i<x;i++){
       for(j=0;j<x;j++){ 
            //various cases for where we are in the grid
    
            //first element in x & y
            if (i==0 && j==0){

		xval = V(0,0) + alpha*(V(0,2) - 2*V(0,1) + V(0,0))/(deltax*deltax);
		yval = V(0,0) + alpha*(V(2,0) - 2*V(1,0) + V(0,0))/(deltay*deltay);
		
		printf("top left corner \n");
		printf("xval %f \n", xval);
		
		//can avg or simply add the two values, will avg
		V(0,0) = abs((xval+yval)/2);
            }
            //last element in x & y 
            else if (i==x-1 && j==y-1){

		xval = V(x-1,y-1) + alpha*(V(x-1,y-1) - V(x-1,y-2)*2 + V(x-1,y-3))/(deltax*deltax);
		yval =  V(x-1,y-1) + alpha*(V(x-1,y-1) - V(x-2,y-1)*2 + V(x-3,y-1))/(deltay*deltay);
		V(x-1,y-1) = abs((xval+yval)/2);
            	
		printf("bottom right corner \n");
		printf("xval %f \n", xval);
		
		}
            //first element(forward) in x, central in iy, left edge
            //else if (i==0 && j!=0 && j!=y){
            else if(j==0){
		xval = V(i,0)+ alpha*(V(i,2) - (V(i,1))*2 + V(i,0))/(deltax*deltax);
		yval = V(i,j)+ alpha*(V(i+1,0) - 2*V(i,0) + V(i-1,0))/(deltay*deltay); //this may break down because of 2*V[i][j] since V is a 2d array or pointer to array 
		V(i,0) = abs((xval+yval)/2);
		printf("left edge \n");
		printf("xval %f \n", xval);
		
	    }
            //last element (backwards) in x, central in y, right edge
            //else if (i==x && j!=0 && j!=y){
            else if (j==y-1){
		xval = V(i,y-1)+alpha*(V(i,j) - V(i,y-2)*2 - V(i,y-3))/(deltax*deltax);
		yval = V(i,y-1)+alpha*(V(i+1,y-1) - 2*V(i,y-1) + V(i-1,y-1))/(deltay*deltay); //this may break down because of 2*V[i][j] since V is a 2d array or pointer to array
		V(i,y-1)=abs((xval+yval)/2);
            
		printf("right edge \n");
		printf("xval %f \n", xval);

	    }

            //first element(forward) in y, central in x, top edge
            //else if (i!=0 && i!=x && j==0)
            else if (i==0) {

		xval = V(0,j)+alpha*(V(0,j+1) - 2*V(0,j) + V(0,j-1))/(deltax*deltax);
		yval = V(0,j)+alpha*(V(2,j) - V(1,j)*2 + V(0,j))/(deltay*deltax);
		V(0,j) = abs((xval+yval)/2); 
            
		printf("top edge \n");
		printf("xval %f \n", xval);
		

		}
            //last element(backwards) in y, central in x, bottom edge
           // else if (i!=0 && i!=x && j==y){
           else if (i==x-1){

		xval = V(x-1,j)+alpha*(V(x-1,j+1) - 2*V(x-1,j) + V(x-1,j-1))/(deltax*deltax);
		yval = V(x-1,j)+alpha*(V(x-1,j) - V(x-2,j)*2 - V(x-3,j))/(deltay*deltay);
		V(x-1,j) = abs((xval+yval)/2); 
            
		printf("bottom edge \n");
		printf("xval %f \n", xval);
		

		}
            //central element (default case)
            else{
		xval = V(i,j)+alpha*(V(i,j+1) - 2*V(i,j) + V(i,j-1))/(deltax*deltax);
		yval = V(i,j)+alpha*(V(i+1,j) - 2*V(i,j) + V(i-1,j))/(deltay*deltay);
		V(i,j) = abs((xval+yval)/2); 

		printf("middle \n");
		printf("xval %f \n", xval);
	
            }
        }
    }
}
}
else{
W(0,0) = 10* W(0,0);
for(k=0;k<tsteps;k++){
    for(i=0;i<x;i++){
       for(j=0;j<y;j++){ 
            //various cases for where we are in the grid
    
            //first element in x & y
            if (i==0 && j==0){

		xval = W(0,0) + alpha*(W(0,2) - 2*W(0,1) + W(0,0))/(deltax*deltax);
		yval = W(0,0) + alpha*(W(2,0) - 2*W(1,0) + W(0,0))/(deltay*deltay);
		
		printf("top left corner \n");
		printf("xval %f \n", xval);
		
		//can avg or simply add the two values, will avg
		V(0,0) = abs((xval+yval)/2);
            }
            //last element in x & y 
            else if (i==x-1 && j==y-1){

		xval = W(x-1,y-1) + alpha*(W(x-1,y-1) - W(x-1,y-2)*2 + W(x-1,y-3))/(deltax*deltax);
		yval =  W(x-1,y-1) + alpha*(W(x-1,y-1) - W(x-2,y-1)*2 + W(x-3,y-1))/(deltay*deltay);
		W(x-1,y-1) = abs((xval+yval)/2);
            	
		printf("bottom right corner \n");
		printf("xval %f \n", xval);
		
		}
            //first element(forward) in x, central in iy, left edge
            //else if (i==0 && j!=0 && j!=y){
            else if(j==0){
		xval = W(i,0)+ alpha*(W(i,2) - (W(i,1))*2 + W(i,0))/(deltax*deltax);
		yval = W(i,j)+ alpha*(W(i+1,0) - 2*W(i,0) + W(i-1,0))/(deltay*deltay); //this may break down because of 2*V[i][j] since V is a 2d array or pointer to array 
		W(i,0) = abs((xval+yval)/2);
		printf("left edge \n");
		printf("xval %f \n", xval);
		
	    }
            //last element (backwards) in x, central in y, right edge
            //else if (i==x && j!=0 && j!=y){
            else if (j==y-1){
		xval = W(i,y-1)+alpha*(W(i,j) - W(i,y-2)*2 - W(i,y-3))/(deltax*deltax);
		yval = W(i,y-1)+alpha*(W(i+1,y-1) - 2*W(i,y-1) + W(i-1,y-1))/(deltay*deltay); //this may break down because of 2*V[i][j] since V is a 2d array or pointer to array
		W(i,y-1)=abs((xval+yval)/2);
            
		printf("right edge \n");
		printf("xval %f \n", xval);

	    }

            //first element(forward) in y, central in x, top edge
            //else if (i!=0 && i!=x && j==0)
            else if (i==0) {

		xval = W(0,j)+alpha*(W(0,j+1) - 2*W(0,j) + W(0,j-1))/(deltax*deltax);
		yval = W(0,j)+alpha*(W(2,j) - W(1,j)*2 + W(0,j))/(deltay*deltax);
		W(0,j) = abs((xval+yval)/2); 
            
		printf("top edge \n");
		printf("xval %f \n", xval);
		

		}
            //last element(backwards) in y, central in x, bottom edge
           // else if (i!=0 && i!=x && j==y){
           else if (i==x-1){

		xval = W(x-1,j)+alpha*(W(x-1,j+1) - 2*W(x-1,j) + W(x-1,j-1))/(deltax*deltax);
		yval = W(x-1,j)+alpha*(W(x-1,j) - W(x-2,j)*2 - W(x-3,j))/(deltay*deltay);
		W(x-1,j) = abs((xval+yval)/2); 
            
		printf("bottom edge \n");
		printf("xval %f \n", xval);
		

		}
            //central element (default case)
            else{
		xval = W(i,j)+alpha*(W(i,j+1) - 2*W(i,j) + W(i,j-1))/(deltax*deltax);
		yval = W(i,j)+alpha*(W(i+1,j) - 2*W(i,j) + W(i-1,j))/(deltay*deltay);
		W(i,j) = abs((xval+yval)/2); 

		printf("middle \n");
		printf("xval %f \n", xval);
	
            }
        }
    }
}
}
/*
for (i=0;i<x;i++){
    for(j=0;j<y;j++){
      V(i,j) = alpha * V(i,j)*0.5;
    }
}
*/

    //return a;
}	


