#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "initial_conditions.h"
#define IC(i,j) ic[(i) + (j)*n]
void initial_conditions(double* ic, int n, int ic_type){
    int i,j;
    //top left
    if(ic_type ==0){
        for(i=0;i<n;i++){
            for(j=0;j<n;j++){
                IC(i,j) = 0;  
            }
        }
        IC(0,0) = 100;
    }
    //sparse diagonal random
    else if(ic_type==1){
        for(i=0;i<n;i++){
            for(j=0;j<n;j++){
                if(i==j){
                    IC(i,j) = rand()%100;
                }
                else(IC(i,j) = 0);
            }
        }
    }
    //10% random
    else if(ic_type==2){
        for(i=0;i<n;i++){
            for(j=0;j<n;j++){
                if(j%(10)==0){
                    IC(i,j) = rand()%100;  
                }
                else(IC(i,j) = 0);
            }
        }
    }
}	


